// Adding items in an array without using methods:
let array = [];
console.log(array[0]);
array[0] = 'Cloud Strife';
console.log(array);
console.log(array[1]);
array[1] = 'Tifa Lockhart';
console.log(array[1]);
console.log(array);
array[array.length-1] = 'Aerith Gainsborough';
console.log(array);
array[array.length] = 'Vincent Valentine';
console.log(array);

// Array Methods
	// Manipulate array with pre-determined JS Functions
	// Mutators - these arrays methods usually change the original array
	
	// let array1 = ['Juan', 'Pedro', 'Jose', 'Andres'];
	// // without method
	// array1[array.length] = 'Francisco';
	// console.log(array1);

	// // .push() - allows us to add an element at the end of the array
	// array1.push('Andres');
	// console.log(array1);

	// // .unshift() - allows us to add an element at the beginning of the array
	// array1.unshift('Simon');
	// console.log(array1);

	// // .pop() - allows us to delete or remove the last item/element at the end of the array
	// array1.pop();
	// console.log(array1);
	// // .pop() is also able to return the item we removed.
	// console.log(array1.pop());
	// console.log(array1);

	// let removedItem = array1.pop();
	// console.log(array1);
	// console.log(removedItem);

	// // .shift() return the item we removed
	// let removedItemShift = array1.shift();
	// console.log(array1);
	// console.log(removedItemShift);


let array1 = ['Juan', 'Pedro', 'Jose', 'Andres'];

let remove = array1.shift(); 

console.log(array1);
console.log(remove);

array1.unshift('George');
console.log(array1);

array1.push('Michael');
console.log(array1);
